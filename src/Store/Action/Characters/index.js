import {SET_CHARACTERS} from "./types";

export const setCharacters = (data) => {
    return {
        type: SET_CHARACTERS,
        payload: data,
    };
};
