import {SET_JWT} from "./types";

export const setJwt = (data) => {
    return {
        type: SET_JWT,
        payload: data,
    };
};
