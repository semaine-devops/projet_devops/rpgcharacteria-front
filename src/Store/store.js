import {combineReducers} from 'redux';
import {JwtReducer} from "./Reducer/JwtReducer";
import {CharactersReducer} from "./Reducer/CharactersReducer";

export const rootReducer = combineReducers({
    jwt: JwtReducer,
    characters: CharactersReducer,
});
