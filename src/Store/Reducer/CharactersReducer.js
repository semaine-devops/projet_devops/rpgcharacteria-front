import {SET_CHARACTERS} from "../Action/Characters/types";

const initialState = {
    characters: [],
}

export function CharactersReducer(state = initialState, action) {
    switch(action.type){
        case SET_CHARACTERS:
            console.log('CHARACTER REDUCER')
            return { ...state, characters: action.payload };
        default:
            return state
    }
}
