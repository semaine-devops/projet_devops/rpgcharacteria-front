import {SET_JWT} from "../Action/JWT/types";

const initialState = {
    jwt: null,
}

export function JwtReducer(state = initialState, action) {
    switch(action.type){
        case SET_JWT:
            console.log('JWT REDUCER')
            return { ...state, jwt: action.payload };
        default:
            return state
    }
}
