import {fireEvent, render, screen} from '@testing-library/react';
import App from '../App';
import {BrowserRouter} from "react-router-dom";

//test block
describe("App", () => {
    it("renders correctly", async () => {
        render(
            <BrowserRouter>
                <App />
            </BrowserRouter>
        );
    });
});
