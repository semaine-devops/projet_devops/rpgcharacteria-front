import React from 'react';
import "./Footer.css"
import {useNavigate} from "react-router-dom";

export const Footer = () => {
    return (
        <div className="FooterBlock">
            <footer className="Footer">
                <h2 className="Text">RPG Character Generator</h2>
                <p className="Text">Alyssia Prévoté--Hauguel, Anis Bastide, Jimmy Lai, Houssam Laghzil, Hugo Monnerie</p>
            </footer>
        </div>
    )
}
