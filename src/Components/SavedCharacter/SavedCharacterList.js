import React from 'react';
import {Header} from "../Header/Header";
import {Footer} from "../Footer/Footer";
import "./SavedCharacter.css"
import { Link, Outlet } from "react-router-dom"
import {useSelector} from "react-redux";

export const SavedCharacterList = () => {
    const characters = useSelector((state) => state.characters.characters)

    return (
        <div>
            <Header />
            <div className="Table">
                <table>
                    <tbody>
                    {characters.map((item, i) => {
                        return [
                                <tr key={i}>
                                    <td className="tdImg">
                                        <img
                                            className="PhotoTable"
                                            src={item.image}
                                            alt=""
                                        /></td>
                                    <td className="tdName"><h2>{item.name}</h2></td>
                                    <td className="tdName"><h2>{item.age}</h2></td>
                                    <td className="tdName"><h2>{item.gender}</h2></td>
                                    <td className="tdName"><h2>{item.characterClass}</h2></td>
                                    <td className="tdName"><h2>{item.characterrace}</h2></td>
                                    <td className="tdName"><h2>{item.history}</h2></td>
                                </tr>
                        ];
                    })}
                    </tbody>
                </table>
            </div>
            <Footer />
            <Outlet />
        </div>
    )
}
