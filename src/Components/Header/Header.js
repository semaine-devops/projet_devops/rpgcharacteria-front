import React from 'react';
import {useNavigate} from "react-router-dom";
import "./Header.css";
import {useDispatch, useSelector} from "react-redux";
import {setJwt} from "../../Store/Action/JWT";
import {setCharacters} from "../../Store/Action/Characters";


export const Header =  () => {

    const jwt =  useSelector((state) => state.jwt.jwt)
    const dispatch = useDispatch()


    const navigate = useNavigate();

    const returnHome = () => {
        navigate('/');
    }

    const NavigateToSignUpPage = () => {
        navigate('/signup');
    }

    const NavigateToSignInPage = () => {
        navigate('/signin');
    }

    const NavigateToSavedCharacter = async () => {
        await GetCharacters()
        navigate('/savedCharacter');
    }
    const Disconnect = () => {
        dispatch(setJwt(null))
    }
    const GetCharacters = async () => {
        const response = await fetch(process.env.REACT_APP_IP + ':3005/getbook', {
            headers: {
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({jwtToken:jwt})
        })
        const responseBody = await response.json()
        dispatch(setCharacters(responseBody.charactersList))
    }

    return (
        <header className="HeaderDesign">
            <div>
                <h1 className="Text" onClick={returnHome}>Voiture</h1>
            </div>
            <div className="ButtonBlock">
                {!jwt && <button className="Button" onClick={NavigateToSignUpPage}>Sign Up</button>}
                {!jwt  && <button className="Button" onClick={NavigateToSignInPage}>Sign In</button>}
                {jwt && <button className="Button" onClick={NavigateToSavedCharacter}>Saved Character</button>}
                {jwt && <button className="Button" onClick={Disconnect}>Disconnect</button>}
            </div>
        </header>
    )
}
