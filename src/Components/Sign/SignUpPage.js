import React, {useState} from 'react';
import {Header} from "../Header/Header";
import "./Sign.css"
import {Footer} from "../Footer/Footer";
import {setJwt} from "../../Store/Action/JWT";
import {setCharacters} from "../../Store/Action/Characters";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";




export const SignUpPage = () => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [errorPass, setErrorPass] = useState('')
    const [errorMail, setErrorMail] = useState('')
    const [colorInput, setColorInput] = useState('')

    const mailRegex = new RegExp("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$")
    const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
    const dispatch = useDispatch()

    const CheckRegister = (event) => {
        event.preventDefault()
        let checkMail = false
        let checkPassword = false
        if (mailRegex.test(email)) {
            checkMail = true
            setErrorMail('')
        } else {
            setErrorMail('Invalid mail address')
        }
        if (password === confirmPassword) {
            checkPassword = true
            setErrorPass('')
        } else {
            setErrorPass('Not same password')
        }
        ConfirmRegister(checkMail, checkPassword)
    }

    const ConfirmRegister = async (checkMail, checkPassword) => {
        if (checkMail && checkPassword) {
            console.log('registerfront')

            if (password === confirmPassword) {
                console.log('confirmpassword')
                const response = await fetch(process.env.REACT_APP_IP + ':3001/signup', {
                    headers: {
                        "Content-Type": "application/json",
                    },
                    method: "POST",
                    body: JSON.stringify({email: email, password: password})
                })
                const responseBody = await response.json()
                dispatch(setJwt(responseBody.jwt))
            }
        }
        GetCharacters()
    }

    const GetCharacters = () => {
        dispatch(setCharacters())
    }

    const VerifyPasswordStruct = () => {
        if (password.length - 1 > 0) {
            if (strongRegex.test(password)) {
                setColorInput("#0F9D58");
            } else if (mediumRegex.test(password)) {
                setColorInput("#F4B400");
            } else {
                setColorInput("#DB4437");
            }
        } else {
            setColorInput("#ffffff");
        }
    }

    const navigate = useNavigate();

    const returnHome = () => {
        navigate('/');
    }

    return (
        <div>
            <Header/>
            <form onSubmit={CheckRegister}>
                <div className="Sign">
                    <div>
                        <label>
                            Email Address:
                            <input type="email" onChange={e => setEmail(e.target.value)}/>
                        </label>
                    </div>

                    <div>
                        <p className="Error">{errorMail}</p>
                    </div>

                    <div className="Password">
                        <label>
                            Password:
                            <input
                                type="password"
                                onChange={e => {
                                    setPassword(e.target.value);
                                    VerifyPasswordStruct()
                                }}
                                style={{
                                    backgroundColor: colorInput,
                                }}
                            />
                        </label>
                    </div>

                    <div>
                        <label>
                            Confirm Password:
                            <input type="password" onChange={e => setConfirmPassword(e.target.value)}/>
                        </label>
                    </div>

                    <div>
                        <p className="Error">{errorPass}</p>
                    </div>

                    <div className="ButtonBlock">
                        <input className="buttonvalid" type="submit" value="Validate"/>
                    </div>

                </div>
            </form>
            <Footer/>
        </div>
    )
}
