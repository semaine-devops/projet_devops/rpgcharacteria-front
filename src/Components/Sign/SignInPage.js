import React, {useState} from 'react';
import {Header} from "../Header/Header";
import "./Sign.css"
import {Footer} from "../Footer/Footer";
import {useDispatch, useSelector} from "react-redux";
import {setJwt} from "../../Store/Action/JWT";
import {setCharacters} from "../../Store/Action/Characters";
import {useNavigate} from "react-router-dom";


export const SignInPage = () => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch()

    const ConfirmConnection = async (event) => {
        event.preventDefault()
        console.log('loginfront')
        const response = await fetch(process.env.REACT_APP_IP + ':3001/login', {
            headers: {
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({email: email, password: password})
        })
        const responseBody = await response.json()
        dispatch(setJwt(responseBody.jwt))
    }


    function getuid() {
        const jwt = localStorage.getItem('jwt')
        console.log(jwt)
        if (jwt) {
            const jwtData = jwt.split('.')[1]
            const decodedJwtJsonData = window.atob(jwtData)
            const decodedJwtData = JSON.parse(decodedJwtJsonData)
            const uid = decodedJwtData.userId
            return uid
        }
    }
    const navigate = useNavigate();

    const returnHome = () => {
        navigate('/');
    }



    return (
        <div>
            <Header/>
            <form onSubmit={ConfirmConnection}>
                <div className="Sign">
                    <div>
                        <label>
                            Email Address:
                            <input type="email" onChange={e => setEmail(e.target.value)}/>
                        </label>
                    </div>
                    <div>
                        <label>
                            Confirm Password:
                            <input type="password" onChange={e => setPassword(e.target.value)}/>
                        </label>
                    </div>
                    <div className="ButtonBlock">
                        <input className="buttonvalid" type="submit" value="Validate"/>
                    </div>
                </div>
            </form>
            <Footer/>
        </div>
    )
}

export default SignInPage
