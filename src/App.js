import './App.css';
import {useEffect, useState} from "react";
import {Header} from "./Components/Header/Header";
import {Footer} from "./Components/Footer/Footer";
import {useSelector} from "react-redux";


function App() {
    const jwt = useSelector((state) => state.jwt.jwt)
    const genders = [
        'Male', 'Female'
    ];
    const classes = [
        'Knight', 'Warrior', 'Archer', 'Mage', 'Berserker', 'Rogue', 'Priest'
    ];
    const races = [
        'Human', 'Elf', 'Dwarf', 'Goblin'
    ];
    const physiques = [
        'one-eyed', 'burned', 'cicatrice', 'wooden_leg', 'normal',
    ];
    const defaultGender = genders[0];
    const defaultClasse = classes[0];
    const defaultRace = races[0];
    const defaultphysique = physiques[0];

    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [race, setRace] = useState(defaultRace)
    const [physique, setphysique] = useState(defaultphysique)
    const [classe, setClasse] = useState(defaultClasse)
    const [gender, setGender] = useState(defaultGender)
    const [description, setDescription] = useState('')
    const [maxAge, setMaxAge] = useState(130)
    const [imgUrl, setImgUrl] = useState('')
    const [isLoading, setIsloading] = useState(false)

    useEffect(() => {
        switch (race) {
            case 'Human':
                setMaxAge(130)
                break
            case 'Elf':
                setMaxAge(2000)
                break
            case 'Dwarf':
                setMaxAge(450)
                break
            case 'Orc':
                setMaxAge(100)
                break
            case 'Goblin':
                setMaxAge(90)
                break
            default:
                setMaxAge(130)
        }
    }, [race])
    useEffect(() => {
        switch (physique) {
            case 'one-eyed':
                setMaxAge(130)
                break
            case 'burned':
                setMaxAge(2000)
                break
            case 'cicatrice':
                setMaxAge(450)
                break
            case 'wooden_leg':
                setMaxAge(100)
                break
            case 'normal':
                setMaxAge(90)
                break
            default:
                setMaxAge(130)
        }
    }, [physique])

    async function getCharacterBackground() {
        if (!jwt) {
            return;
        }
        console.log('BACKGROUND',jwt)
        return await fetch(process.env.REACT_APP_IP + ':3002/generate/', {
            headers: {
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({
                name: name,
                age: age,
                gender: gender,
                characterClass: classe,
                race: race,
                physique: physique,
                jwtToken: jwt,
                img: imgUrl,
                history: description,
            }),
        })
    }

    async function getCharacterImage() {
        console.log('IMAGE',jwt)
        return await fetch(process.env.REACT_APP_IP + ':3003/generate', {
            headers: {
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({
                age: age,
                gender: gender,
                characterClass: classe,
                race: race,
                physique: physique,
                jwtToken: jwt
            })
        })
    }

    async function getQuota(){
        console.log('QUOTA',jwt)
        return await fetch(process.env.REACT_APP_IP + ':3004/quota',{
            headers: {
                "Content-Type": "application/json",
            },
            method: "POST",
            body:JSON.stringify({
                jwtToken:jwt
            })
        })
    }
    const Generate = async e => {
        console.log("generate")
        if (!jwt) {
            return;
        }
        setIsloading(true)
        console.log('quota1')

        const quotaResponse = await getQuota()
        const quotaJson = await quotaResponse.json()
        const quota = quotaJson.quota
        console.log('quota2')
        if (quota===0){
            setIsloading(false)
            console.log("quota de zéro")
            return
        }

        console.log("loding")
        const story = await getCharacterBackground()
        const imgUrl = await getCharacterImage()
        const storyResult = await story.json()
        const imgUrlResult = await imgUrl.json()
        setDescription(storyResult.result)
        setImgUrl(imgUrlResult.result)
        setIsloading(false)
        console.log("not loding")

        if (isLoading === false) {
            console.log("pushToHistory")

            return await fetch(process.env.REACT_APP_IP + ':3005/postbook', {
                headers: {
                    "Content-Type": "application/json",
                },
                method: "POST",
                body: JSON.stringify({
                    jwtToken: jwt,
                    name: name,
                    age: age,
                    gender: gender,
                    characterClass: classe,
                    image: imgUrlResult.result,
                    characterrace: race,
                    physique: physique,
                    history: storyResult.result,
                })
            });

        }
    }

    return (
        <div className="alldiv">
            <Header/>
            <div className="App">
                <div className="PhotoBlock">
                        <img
                            className="Photobackgroud"
                            src={imgUrl}
                            alt={name}
                        /><img
                            className="Photo"
                            src={imgUrl}
                            alt={name}
                        />
                </div>
                <div className="CharacterInfoBlock">
                    <div>
                        <h3>Name</h3>
                        <input type="text" value={name} onChange={e => setName(e.target.value)}/>
                    </div>
                    <div>
                        <h3>Age</h3>
                        <input type="number" value={age} onChange={e => setAge(e.target.value)} min="0" max={maxAge}/>
                    </div>
                    <div>
                        <h3>Race</h3>
                        <select
                            onChange={(e) => setRace(e.target.value)}
                            defaultValue={defaultRace}
                        >
                            {races.map((option, idx) => (
                                <option key={idx}>{option}</option>
                            ))}
                        </select>
                    </div>
                    <div>
                        <h3>physique</h3>
                        <select
                            onChange={(e) => setphysique(e.target.value)}
                            defaultValue={defaultphysique}
                        >
                            {physiques.map((option, idx) => (
                                <option key={idx}>{option}</option>
                            ))}
                        </select>
                    </div>
                    <div>
                        <h3>Gender</h3>
                        <select
                            onChange={(e) => setGender(e.target.value)}
                            defaultValue={defaultGender}
                        >
                            {genders.map((option, idx) => (
                                <option key={idx}>{option}</option>
                            ))}
                        </select>
                    </div>
                    <div>
                        <h3>Class</h3>
                        <select
                            onChange={(e) => setClasse(e.target.value)}
                            defaultValue={defaultClasse}
                        >
                            {classes.map((option, idx) => (
                                <option key={idx}>{option}</option>
                            ))}
                        </select>
                    </div>
                    {jwt && !isLoading && <button className="Button" onClick={Generate}>Validate</button>}

                </div>
            </div>
            <div className="DescriptionBlock">
                <h2>Description</h2>
                <p>{description}</p>
            </div>
            <Footer/>
        </div>
    );
}

export default App;
