import React from 'react';
import { Routes, Route } from 'react-router-dom';
import App from "../App";
import {SignUpPage} from "../Components/Sign/SignUpPage";
import {SignInPage} from "../Components/Sign/SignInPage";
import {SavedCharacterList} from "../Components/SavedCharacter/SavedCharacterList";

export default function Router() {
    return (
        <Routes>
            <Route path="/" element={<App />} />
            <Route path="signup" element={<SignUpPage />} />
            <Route path="signin" element={<SignInPage />} />
            <Route path="/savedCharacter" element={<SavedCharacterList />}/>
        </Routes>
    )
}
